#!/bin/bash

git pull

if [[ ! $1 ]]; then
  set -- local
fi

echo 'Making deployement for :' $1
ng build --configuration=$1

echo 'Moving htaccess to dist file'
cp .htaccess dist/inventaire-front/

# If using linux for apache
echo 'Relaod apache2'
systemctl reload apache2

echo 'All done'