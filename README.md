# HiveApp

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.23.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

# My config

## Install last version of angular (8.1.0)
npm install -g @angular/cli

## Start new project
ng new hiveApp
or
ng new hiveApp --skip-install=true
cd hiveApp
npm install

## Install moment js:
npm install moment --save
Link: https://momentjs.com/

## Install material:
ng add @angular/material
npm i @angular/platform-browser
npm i @angular/material @angular/cdk @angular/animations
npm i @angular/material-moment-adapter
Link: https://monpetitdev.fr/angular-datepicker-material/
https://material.angular.io/guide/getting-started
https://material.angular.io/components/datepicker/examples (change langage)

## Build for integration | production
ng build --inte | ng build --prod
ng build --configuration= integration | production

## Add script to deploy
./deploy.sh <env> //inte|prod

## Run script command from package.json
npm run-script <commande> //start

## Install traduction:
npm install @ngx-translate/core --save
npm install @ngx-translate/http-loader --save

## Help find translation in the good file
Link: https://github.com/ngx-translate/core

## Install module for cookies:
npm install ngx-cookie-service --save
Link: https://www.npmjs.com/package/ngx-cookie-service

## Install http:
npm install @angular/http@latest

## Add new component
ng g component <name>

## Add testing check if all the components are runing well
ng test