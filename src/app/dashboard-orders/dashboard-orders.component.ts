import { Component, OnInit } from '@angular/core';
import { DashboardService, FrontService } from './../../_services/index';

@Component({
  selector: 'hive-dashboard-orders',
  templateUrl: './dashboard-orders.component.html'
})
export class DashboardOrdersComponent implements OnInit {
  public orders;
  constructor(
    private _dashboardService: DashboardService,
    private _frontService: FrontService
  )
  { }

  ngOnInit() {
    this.getOrders();
  }

  /**
   * Get Orders
   */
  public getOrders() {
    this._dashboardService.getElements('orders')
      .subscribe(
        res => {
          if(!res.error){
            this.orders = res;
          }else{
            this._frontService.errDisplay({message: res.reason});
          }
        },
        (err) => {
          this._frontService.errDisplay(err);
        }
      )
  }

}
