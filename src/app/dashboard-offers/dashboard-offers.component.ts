import { Component, OnInit } from '@angular/core';
import { DashboardService, FrontService } from './../../_services/index';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'hive-dashboard-offers',
  templateUrl: './dashboard-offers.component.html'
})
export class DashboardOffersComponent implements OnInit {
  public offers;
  constructor(
    private _dashboardService: DashboardService,
    private _frontService: FrontService
  )
  { }

  ngOnInit() {
    this.getOffers();
  }

  /**
   * Get Offers
   */
  public getOffers() {
    this._dashboardService.getElements('offers')
      .subscribe(
        res => {
          if(!res.error){
            this.offers = res;
          }else{
            this._frontService.errDisplay({message: res.reason});
          }
        },
        (err) => {
          this._frontService.errDisplay(err);
        }
      )
  }

}
