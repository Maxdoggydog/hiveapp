import { Component, OnInit } from '@angular/core';
import { DashboardService, FrontService } from './../../_services/index';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'hive-dashboard-users',
  templateUrl: './dashboard-users.component.html'
})
export class DashboardUsersComponent implements OnInit {
  public users;
  constructor(
    private _dashboardService: DashboardService,
    private _frontService: FrontService
  )
  { }

  ngOnInit() {
    this.getUsers();
  }

  /**
   * Get Users
   */
  public getUsers() {
    this._dashboardService.getElements('users')
      .subscribe(
        res => {
          if(!res.error){
            this.users = res;
          }else{
            this._frontService.errDisplay({message: res.reason});
          }
        },
        (err) => {
          this._frontService.errDisplay(err);
        }
      )
  }

}
