import { Component, OnInit } from '@angular/core';
import { LogInService, DashboardService, FrontService } from './../../_services/index';
import { Router } from '@angular/router';

@Component({
  selector: 'hive-dashboard',
  templateUrl: './dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  constructor(
    private _logInService: LogInService,
    private _router: Router,
  )
  { }

  ngOnInit() {

  }

  /**
   * Log out
   */
  public logout(): void {
    this._logInService.logout();
    this._router.navigate(['/']);
  }
}