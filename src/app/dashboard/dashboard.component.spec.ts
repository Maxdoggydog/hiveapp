import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { DashboardComponent } from './dashboard.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { NotifierModule } from 'angular-notifier';
import { CookieService } from 'ngx-cookie-service';
import { LogInService, DashboardService, AuthService, FrontService } from './../../_services/index';

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  let compiled: any;


  function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/_i18n/', '.json');
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: createTranslateLoader,
            deps: [HttpClient]
          }
        }),
        HttpModule,
        NotifierModule
      ],
      declarations: [
        DashboardComponent
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        CookieService, LogInService, DashboardService, AuthService, FrontService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the DashboardComponent', () => {
    expect(component).toBeTruthy();
  });

  it(`should create 'check good page HIVEAPP'`, () => {
    expect(compiled.querySelector('a.white-link img.logo').textContent).toBeTruthy();
  });

  it(`Check if the table exist`, () => {
    expect(compiled.querySelector('div.table-part table')).toBeTruthy();
  });

});
