import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard, AuthGuard2 } from './../_guards/auth.guard'

import { AppComponent } from './app.component'
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DashboardUsersComponent } from './dashboard-users/dashboard-users.component';
import { DashboardOffersComponent } from './dashboard-offers/dashboard-offers.component';
import { DashboardOrdersComponent } from './dashboard-orders/dashboard-orders.component';

const routes: Routes = [
  {
    path: '',
    component: LoginComponent, canActivate: [AuthGuard2],
  },{
    path: 'login',
    component: LoginComponent, canActivate: [AuthGuard2],
  },
  {
    path: 'dashboard',
    component: DashboardComponent, canActivate: [AuthGuard],
    children: [
      {
        path: 'users',
        component: DashboardUsersComponent/* , canActivate: [AuthGuard2] */,
      },{
        path: 'offers',
        component: DashboardOffersComponent/* , canActivate: [AuthGuard2] */,
      },{
        path: 'orders',
        component: DashboardOrdersComponent/* , canActivate: [AuthGuard2] */,
      }
    ]
  },
  {
    path: '**',
    component: LoginComponent, canActivate: [AuthGuard2]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
