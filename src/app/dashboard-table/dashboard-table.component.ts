import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { LogInService, DashboardService, FrontService } from './../../_services/index';
import { Router } from '@angular/router';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort } from '@angular/material/sort';
import { DataSource } from '@angular/cdk/collections';
import { Observable, of } from 'rxjs';
import { animate, state, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'hive-dashboard-table',
  templateUrl: './dashboard-table.component.html',
  animations: [
    trigger('detailExpand', [
      state('collapsed', style({ height: '0px', minHeight: '0', display: 'none' })),
      state('expanded', style({ height: 'height: 100%;', display: 'block' })),
      transition('expanded <=> collapsed', animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')),
    ]),
  ],
})
export class DashboardTableComponent implements OnInit {
  public displayedColumns: string[] = ['thisMonth', 'lastMonth', 'thisWeek', 'lastWeek'];
  public dataSource = new MatTableDataSource<any>();

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  public isExpansionDetailRow = (i: number, row: Object) => row.hasOwnProperty('detailRow');
  public expandedElement: any;

  expandedRow: number;

  @Input() data: any;
  @Input() TYPE: any;

  constructor(
    private _logInService: LogInService,
    private _dashboardService: DashboardService,
    private _router: Router,
    private _frontService: FrontService
  ) { }

  ngOnInit() {
    // this.dataSource.data = [this.data]; // object not array
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.connect();
  }

  public connect(): Observable<Element[]> {
    this.data = [this.data];

    const rows = [];
    this.data.forEach(element => rows.push(element, { detailRow: true, element }));
    console.log(rows);
    this.dataSource.data = rows;
    return of(rows);
  }

}
