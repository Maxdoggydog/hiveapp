import { Component, OnInit } from '@angular/core';
import { User } from './../../_classes/index';
import { AuthService, LogInService, FrontService } from './../../_services/index';
import { NgForm, FormGroup, FormBuilder, Validators  } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'hive-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {
  public user: User = new User();
  public form: FormGroup;
  public disabled: boolean = false;

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _logInService: LogInService,
    private _authService: AuthService,
    private _frontService: FrontService
  )
  { }

  /**
   * Return the good error
   */
  public getErrorMessage() {
    return this.form.controls.email.hasError('required') ? 'NEED_A_VALUE' : this.form.controls.email.hasError('email') ? 'GOOD_VALUE_FOR_EMAIL' :'';
  }

  ngOnInit() {
    this.initFormControler();
  }

  /**
   * Init formcontrole
   */
  public initFormControler(): void {
    this.form = this._formBuilder.group({
      email: ['',
        [
          Validators.required
        ]
      ],
      password: ['',
        [
          Validators.required
        ]
      ]
    });
  }

  /**
   * Register user
   */
  public login () {
    if(this.form.valid){
      this.disabled = true;
      this._logInService.login(this.form.value)
        .subscribe(
          res => {
            if(!res.error){
              this._router.navigate([this._authService.getRedirectUrl()]);
            }else{
              this._frontService.errDisplay({message: res.reason});
              this.disabled = false;
            }
          },
          (err) => {
            this._frontService.errDisplay(err);
            this.disabled = false;
          }
        )
    }else{
      this.disabled = false;
      this._frontService.errDisplay({message: 'USER_NOT_LOGGED'});
    }
  }

}
