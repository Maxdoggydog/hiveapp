import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from './login.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { NotifierModule } from 'angular-notifier';
import { CookieService } from 'ngx-cookie-service';
import { LogInService, AuthService, FrontService } from './../../_services/index';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let compiled: any;


  function createTranslateLoader(http: HttpClient) {
    return new TranslateHttpLoader(http, './assets/_i18n/', '.json');
  }

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        TranslateModule.forRoot({
          loader: {
            provide: TranslateLoader,
            useFactory: createTranslateLoader,
            deps: [HttpClient]
          }
        }),
        HttpModule,
        NotifierModule,
        ReactiveFormsModule,
        FormsModule
      ],
      declarations: [
        LoginComponent
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
      providers: [
        CookieService, LogInService, AuthService, FrontService
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    fixture.detectChanges();
  });

  it('should create the LoginComponent', () => {
    expect(component).toBeTruthy();
  });

  it(`should create 'check good page LOGIN'`, () => {
    expect(compiled.querySelector('mat-toolbar').textContent).toContain('LOGIN');
  });

  it('form invalid when empty', () => {
    expect(component.form.valid).toBeFalsy();
  });

  it('email field validity', () => {
    let email = component.form.controls['email'];
    expect(email.valid).toBeFalsy();
  });

  it('password field validity', () => {
    let password = component.form.controls['password'];
    expect(password.valid).toBeFalsy();
  });

  it('email field validity error true', () => {
    let errors = {};
    let email = component.form.controls['email'];
    errors = email.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  it(`should have as error required equal to 'true'`, () => {
    let password = component.form.controls['password'];
    password.setValue('');
    let errors = {};
    errors = password.errors || {};
    expect(errors['required']).toBeTruthy();
  });

  it(`should have as form valid to 'true'`, () => {
    let password = component.form.controls['password'];
    password.setValue('maxi1234');

    let email = component.form.controls['email'];
    email.setValue("maxi.mampuya@gmail.com");
    expect(component.form.valid).toBeTruthy();
  });

});
