import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './../_services/auth.service';

import { environment } from './../environments/environment';

/**
 * If not connected
 * Go to the auth page
 */
@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private _authService: AuthService,
    private router: Router
  )
  { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {
    let url: string = state.url;

    if (this._authService.isUserLoggedIn()) {
      return true;
    }
    this._authService.setRedirectUrl(url);
    this.router.navigate([ this._authService.getLoginUrl() ]);
    return false;

  }
}

/**
 * If connected
 * Doesn't go to the auth page
 */
@Injectable()
export class AuthGuard2 implements CanActivate {

  constructor(
    private _authService: AuthService,
    private router: Router
  )
  { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean>|Promise<boolean>|boolean {

    if (this._authService.isUserLoggedIn()) {
      this.router.navigate([ this._authService.getRedirectUrl() ]);
    }
    return true;

  }
}
