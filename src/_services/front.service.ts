import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';
import { NotifierService } from 'angular-notifier';

import { Router } from '@angular/router';

import { environment } from './../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { AuthService } from './auth.service';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class FrontService {
  private options: object = {};

  constructor(
    private _http: Http,
    private _router: Router,
    private _notifier: NotifierService,
    private _cookieService: CookieService,
    private _authService: AuthService,
    private _translate: TranslateService
  ) {
    this.options = {headers : this._authService.headersRequest};
  }

  /**
   * Display errors
   */
  public errDisplay(err: any = {message: ''}, obj: any = null){
    const message = err.message;
    this.messageDisplay('error', message, obj);
  }

  /**
   * Display success
   */
  public successDisplay(success: any = {message: ''}, obj: any = null){
    const message = success.message;
    this.messageDisplay('success', message, obj);
  }

  /**
   * Display warning
   */
  public warningDisplay(warning: any = {message: ''}, obj: any = null){
    const message = warning.message;
    this.messageDisplay('warning', message, obj);
  }

  /**
   * Display message
   */
  public messageDisplay(type: string, message: string, obj: any){
    if(!message){
      return;
    }else{
      this._translate.get(message, obj).subscribe((res: string) => {
        this._notifier.show({
          type: type,
          message: res
        });
      });
    }
  }

}
