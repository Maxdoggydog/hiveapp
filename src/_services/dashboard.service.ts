import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';

import { User } from './../_classes/index';
import { Router } from '@angular/router';

import { environment } from './../environments/environment';
import { AuthService } from './index';


@Injectable()
export class DashboardService {
  private _headers: Headers = new Headers();
  public user: User  = new User();
  private options: any = {};

  constructor(
    private _http: Http,
    private _authService: AuthService
  )
  {
    this.options = {headers : this._authService.headersRequest};
  }

  /**
   * Get elements {users|offers|orders}
   */
  public getElements(type: string): Observable<any> {
    return this._http.get(environment.domain + 'report/'+ type +'/dashboard', this.options)
      .pipe(
        map((res: any) => {
          res = res.json();
          return res;
        }),
        catchError((error: any) => {
          return throwError((error.json()))
        })
      )
  }


}

