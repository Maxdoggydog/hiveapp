import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';

import { User } from './../_classes/index';
import { Router } from '@angular/router';

import { environment } from './../environments/environment';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {
  private redirectUrl: string = '/dashboard';
  private loginUrl: string = '/';


  public user: User  = new User();
  private _headers: Headers = new Headers();

  constructor(
    private _http: Http,
    private _router: Router,
    private _cookieService: CookieService
  ) {


  }

  /**
   * Set the current user
   */
  public setUser(token: string): void {
    if(token){
      this.user.setUser({token: token});
      this._cookieService.set('token', token, null, '/');
      this.headersRequest['X-Access-Token'] = this.user.token;
    }else{
      this.user.setUser(new User());
    }
  }

  /**
   * Reset all param to null
   */
  public resetAllParams(): void {
    this.setUser('');
    this._cookieService.deleteAll();
  }


  public isUserLoggedIn(): boolean {
    return !!this._cookieService.get('token');
  }

  public getRedirectUrl(): string {
    return this.redirectUrl;
  }

  public setRedirectUrl(url: string): void {
    this.redirectUrl = url;
  }

  public getLoginUrl(): string {
    return this.loginUrl;
  }

  public headersRequest: any = {
    'Access-Control-Allow-Origin': environment.domain,
    'Access-Control-Allow-Methods': 'POST, GET, PUT, DELETE, PATCH',
    'Access-Control-Allow-Headers': true,
    'Access-Control-Allow-Credentials': 'Content-type',
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': 'application/json;',
    'withCredentials': true,
    'X-Access-Token': this.authHeader().Authorization,
  };
  private options: object = {headers : this.headersRequest};

  /**
   * Auth for header
   */
  public authHeader() {
    // return authorization header with jwt token
    const token = this._cookieService.get('token') ? this._cookieService.get('token') : '';

    if (token) {
      return { Authorization: token };
    } else {
      return {};
    }
  }


}

