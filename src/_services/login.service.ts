import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions, RequestMethod } from '@angular/http';
import { map, catchError } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { throwError } from 'rxjs';

import { User } from './../_classes/index';
import { Router } from '@angular/router';

import { environment } from './../environments/environment';
import { AuthService } from './index';


@Injectable()
export class LogInService {
  private _headers: Headers = new Headers();
  public user: User  = new User();
  private options: any = {};

  constructor(
    private _http: Http,
    private _authService: AuthService
  )
  {
    this.options = {headers : this._authService.headersRequest};
  }

  /**
   * Log the user
   */
  public login(login_param): Observable<any> {
    const param = `email=${login_param.email}&password=${login_param.password}`;
    return this._http.post(environment.domain + 'user/hiveTokenLogin', param, this.options)
      .pipe(
        map((res: any) => {
          res = res.json();
          if(!res.error){
            this._authService.setUser(res.token);
          }
          return res;
        }),
        catchError((error: any) => {
          return throwError((error.json()))
        })
      )
  }

  /**
   * Log out the user
   */
  public logout(): void {
    this._authService.resetAllParams();
  }

}

