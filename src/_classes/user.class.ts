import { Injectable } from '@angular/core';

@Injectable()
export class User {
  public id: number = 0;
  public email: string = '';
  public token: string = '';
  public password: string = '';
  public password_2: string = '';
  private className: string = 'User';

  /**
   * Create object
   */
  constructor(

  ){

  }

  /**
   * Initialize User object
   */
  setUser (user: any = {}) : User {
    this.id = user.id;
    this.email = user.email;
    this.token = user.token;
    this.password = '';//user.password;
    this.password_2 = '';//user.password_2;
    return this;
  }

  /**
   * Get the class
   */
  getClass() {
    return this.className;
  }
}